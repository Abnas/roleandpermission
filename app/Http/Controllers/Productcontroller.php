<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Product;

class Productcontroller extends Controller
{
    public function viewproduct(){
        return view('Product.product');
    }

    public function addproduct(Request $req){
        $this->validate($req,[
            'name'=>'required',
            
        ]);

        $product=new Product();
        $product->name=$req->name;
        
        $product->save();
        session()->put('success' ,'Successfully added');
        return view('Product.product');
    }

    public function listproduct(){

        $products=Product::all();
        return view('Product.listproduct',['products'=>$products]);
    }

    public function editproduct($id){
        $product=Product::find($id);
        return view('Product.editproduct',['product'=>$product]);
    }

    public function updateproduct($id,Request $req){
        $product=Product::find($id);
        $product->name=$req->name;
        $product->save();
        session()->put('editproduct','Successfully Edited');
        return redirect('productlist');

    }

    public function deleteproduct($id){
        $product=Product::find($id);
        $product->delete();
        session()->put('deleteproduct','Successfully deleted');
        return redirect('productlist');
    }

   
}
