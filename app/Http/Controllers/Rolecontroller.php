<?php

namespace App\Http\Controllers;

use App\Repositories\Rolerepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class Rolecontroller extends Controller
{
    
    protected $rolerepo;

    public function __construct(Rolerepository $rolerepo){
        $this->rolerepo=$rolerepo;
    }


    public function viewrole(){
        return view('Role.role');
    }

    public function addrole(Request $req){
        $success=$this->rolerepo->addrole($req);
        if($success){
        session()->put('success' ,'Successfully added');
            return view('Role.role');
        }
        else{
            return redirect()->back();
        }
       
    }

    public function listrole(){

        $roles=$this->rolerepo->listrole();
        return view('Role.listrole',['roles'=>$roles]);
    }

    public function editrole($id){
    
        
        $val=$this->rolerepo->editrole($id);
        $role=$val["role"];
        $permissions=$val["permissions"];
        // dd($role);
        return view('Role.editrole',['role'=>$role,'permissions'=>$permissions]);
    }

    public function updaterole($id,Request $req){
        $this->rolerepo->updaterole($id,$req);
        session()->put('editrole','Successfully Edited');
        return redirect()->back();

    }

    public function deleterole($id){
        $this->rolerepo->deleterole($id);
        session()->put('deletrole','Successfully deleted');
        return redirect('rolelist');
    }
}


