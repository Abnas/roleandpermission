<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use App\Repositories\Userrepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class Usercontroller extends Controller
{   
    
    protected $userrepo;

    public function __construct(Userrepository $userrepo){
        $this->userrepo=$userrepo;
    }

    public function viewuser(){
        $roles=Role::all();
        return view('User.user')->with('roles',$roles);
    }

    public function adduser(Request $req){
        
            $this->userrepo->adduser($req);
            session()->put('success' ,'Successfully added');
            return redirect('/user');
        
        
    }

    public function listuser(){

        $users=$this->userrepo->listuser();
        return view('User.listuser',['users'=>$users]);
    }

    public function edituser($id){
       
        $values=$this->userrepo->edituser($id);
        $user=$values["user"];
        $role=$values["role"];
        $roles=$values["roles"];
        return view('User.edituser',['user'=>$user,'role'=>$role,'roles'=>$roles]);
    }

    public function updateuser($id,Request $req){
        
        $this->userrepo->updateuser($id,$req);
        session()->put('edituser','Successfully Edited');
        return redirect('userlist');

    }

    public function deleteuser($id){
        $this->userrepo->deleteuser($id);
        session()->put('deletuser','Successfully deleted');
        return redirect('userlist');
    }


    public function logout(){
        Auth::logout();
        return redirect('/');
    }
}
