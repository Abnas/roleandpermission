<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    {{-- <script src="js/jquery-3.5.1.min.js"></script>
<script src="js/bootstrap.min.js"></script> --}}
<script
  src="https://code.jquery.com/jquery-3.5.1.js"
  integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
  crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Company name</a>
     
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="/logout">Sign out</a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column nav-tabs"  id="myTab">
              
              <li class="nav-item">
                <a class="nav-link active" data-toggle="tab"  href="/productlist">
                  <span data-feather="file"></span>
                  Product
                </a>
              </li>
              {{-- <li class="nav-item">
                <a class="nav-link" href="/product">
                  <span data-feather="shopping-cart"></span>
                  Add Product
                </a>
              </li> --}}
              <li class="nav-item">
                <a class="nav-link active" data-toggle="tab"  href="/userlist">
                  <span data-feather="users"></span>
                User
                </a>
              </li>
              {{-- <li class="nav-item">
                <a class="nav-link" href="/user">
                  <span data-feather="bar-chart-2"></span>
                  Add User
                </a>
              </li> --}}
              {{-- @role('editor|admin|visitor') --}}
              <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="/rolelist">
                  <span data-feather="layers"></span>
                  Role
                </a>
              </li>
              {{-- @endrole --}}

              {{-- <li class="nav-item">
                <a class="nav-link" href="/role">
                  <span data-feather="layers"></span>
                  Add Role
                </a>
              </li> --}}
            </ul>
           
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            <h1 class="h2">@yield('header')</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
             
              {{-- <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <span data-feather="calendar">@yield('add')</span> 
              
               

              </button> --}}
              @yield('add')
              
            </div>
          </div>

          {{-- <canvas class="my-4" id="myChart" width="900" height="380"></canvas> --}}

          {{-- <h2>Section title</h2>
          <div class="table-responsive">
            <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Header</th>
                  <th>Header</th>
                  <th>Header</th>
                  <th>Header</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1,001</td>
                  <td>Lorem</td>
                  <td>ipsum</td>
                  <td>dolor</td>
                  <td>sit</td>
                </tr>
                <tr>
                  <td>1,002</td>
                  <td>amet</td>
                  <td>consectetur</td>
                  <td>adipiscing</td>
                  <td>elit</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td>Integer</td>
                  <td>nec</td>
                  <td>odio</td>
                  <td>Praesent</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td>libero</td>
                  <td>Sed</td>
                  <td>cursus</td>
                  <td>ante</td>
                </tr>
                <tr>
                  <td>1,004</td>
                  <td>dapibus</td>
                  <td>diam</td>
                  <td>Sed</td>
                  <td>nisi</td>
                </tr>
                <tr>
                  <td>1,005</td>
                  <td>Nulla</td>
                  <td>quis</td>
                  <td>sem</td>
                  <td>at</td>
                </tr>
                <tr>
                  <td>1,006</td>
                  <td>nibh</td>
                  <td>elementum</td>
                  <td>imperdiet</td>
                  <td>Duis</td>
                </tr>
                <tr>
                  <td>1,007</td>
                  <td>sagittis</td>
                  <td>ipsum</td>
                  <td>Praesent</td>
                  <td>mauris</td>
                </tr>
                <tr>
                  <td>1,008</td>
                  <td>Fusce</td>
                  <td>nec</td>
                  <td>tellus</td>
                  <td>sed</td>
                </tr>
                <tr>
                  <td>1,009</td>
                  <td>augue</td>
                  <td>semper</td>
                  <td>porta</td>
                  <td>Mauris</td>
                </tr>
                <tr>
                  <td>1,010</td>
                  <td>massa</td>
                  <td>Vestibulum</td>
                  <td>lacinia</td>
                  <td>arcu</td>
                </tr>
                <tr>
                  <td>1,011</td>
                  <td>eget</td>
                  <td>nulla</td>
                  <td>Class</td>
                  <td>aptent</td>
                </tr>
                <tr>
                  <td>1,012</td>
                  <td>taciti</td>
                  <td>sociosqu</td>
                  <td>ad</td>
                  <td>litora</td>
                </tr>
                <tr>
                  <td>1,013</td>
                  <td>torquent</td>
                  <td>per</td>
                  <td>conubia</td>
                  <td>nostra</td>
                </tr>
                <tr>
                  <td>1,014</td>
                  <td>per</td>
                  <td>inceptos</td>
                  <td>himenaeos</td>
                  <td>Curabitur</td>
                </tr>
                <tr>
                  <td>1,015</td>
                  <td>sodales</td>
                  <td>ligula</td>
                  <td>in</td>
                  <td>libero</td>
                </tr>
              </tbody>
            </table>
          </div> --}}


          @yield('content')
        </main>
      </div>
    </div>

    <script>
      $(document).ready(function(){
          // $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
          //     localStorage.setItem('activeTab', $(e.target).attr('href'));
          // });
          // var activeTab = localStorage.getItem('activeTab');
          // if(activeTab){
          //     $('#myTab a[href="' + activeTab + '"]').css('background','gray').tab('show');
          // }


          // $('.active').click(function(){
            
          //     // while()
          //     $(this).css('background','gray');

          // });

      //     $('.active').click(function () {
      //     $(this).css('background-color', 'transparent');
      //     $(this).css('background-color', 'red');
      // }).trigger('change');
      // });

    
      </script>

</body>
</html>